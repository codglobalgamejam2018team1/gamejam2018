{
    "id": "a90c0dd4-6cda-4a07-8d3f-ce946c99de77",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_key_S",
    "eventList": [
        {
            "id": "15063567-2ab3-42b1-b8c9-30dd71e2468d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a90c0dd4-6cda-4a07-8d3f-ce946c99de77"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "e243d37d-0be7-4001-94ca-095d8c144212",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "799c4968-ed5b-466e-b82f-7d4b9631b4d1",
    "visible": true
}