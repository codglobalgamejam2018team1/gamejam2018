{
    "id": "030ccd2b-b5d0-4c7a-bd72-30874a4fdfbd",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_key_space",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "e243d37d-0be7-4001-94ca-095d8c144212",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "4f5ff0e5-43a5-4be6-bc2f-024f889accc8",
    "visible": true
}