{
    "id": "0891aa90-4fb4-400e-9570-e21622dad405",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_hitbox3",
    "eventList": [
        {
            "id": "e8e1512f-18fe-49c6-88a6-1ae60f167521",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0891aa90-4fb4-400e-9570-e21622dad405"
        },
        {
            "id": "77582234-bd03-45f5-bbf6-0999d1de126d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "0891aa90-4fb4-400e-9570-e21622dad405"
        },
        {
            "id": "ba50045b-2fe8-43df-b146-54de84c7a4bc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "21b2cc7c-f48e-4996-a126-6189dfda6f1c",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "0891aa90-4fb4-400e-9570-e21622dad405"
        },
        {
            "id": "c8e37007-78bd-48e4-a798-c188decd94c3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "0891aa90-4fb4-400e-9570-e21622dad405"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "6dac4494-3893-4f93-8074-ff2d20a296f1",
    "visible": false
}