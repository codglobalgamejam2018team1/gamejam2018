global.player4_dead_ = false;
global.player4_win_level_ = false;
instance_create_layer(x, y, "Instances", o_hitbox4);
image_speed = 0;
sprite_index = s_virus;
image_xscale = 2;
image_yscale = 2;

hit_ = false;
invincible_ = false;
player_health_ = global.player_max_health_;
player_speed_ = 16;
player_protein_ = 0;
player_can_divide_ = false;
state_ = 1;

right_dir_ = 0;
left_dir_ = 180;

right_input_ = "L";
left_input_ = "K";