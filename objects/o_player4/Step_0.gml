depth = -y;

//If the player dies, kill him and end the event
if player_health_ <= 0
{
	global.player4_dead_ = true;
	audio_play_sound(a_deathFizzle, 10, false);
	instance_destroy(o_hitbox4);
	instance_destroy();
	exit;
}

if player_protein_ >= 3
{
	player_can_divide_ = true;
}
else
{
	player_can_divide_ = false;
}

if state_ == 1
{
	if player_protein_ == 0
	{
		image_index = 0;
	}
	if player_protein_ == 1
	{
		image_index = 1;
	}
	if player_protein_ == 2
	{
		image_index = 2;
	}
}
else if state_ == 2
{
	if player_protein_ == 0
	{
		image_index = 3;
	}
	if player_protein_ == 1
	{
		image_index = 4;
	}
	if player_protein_ == 2
	{
		image_index = 5;
	}
}
else if state_ == 4
{
	if player_protein_ == 0
	{
		image_index = 6;
	}
	if player_protein_ == 1
	{
		image_index = 7;
	}
	if player_protein_ == 2
	{
		image_index = 8;
	}
	if player_protein_ >= 3 and not global.player4_win_level_
	{
		global.player4_win_level_ = true;
	}
}

//Movement inputs
if keyboard_check(ord(right_input_))
{
	move_contact_solid(right_dir_, player_speed_);
}
if keyboard_check(ord(left_input_))
{
	move_contact_solid(left_dir_, player_speed_);
}

//Division input
if keyboard_check_pressed(vk_space)
{
	if player_can_divide_
	{
		if state_ == 1
		{
			event_user(0);
		}
		else if state_ == 2
		{
			event_user(1);
		}
		else if state_ == 4
		{
			event_user(2);
		}
		else exit;
	}
}

// set transparency based on health
image_alpha = (player_health_ * .3) + 0.1;