{
    "id": "cb217794-cf2d-48a5-990a-1e99d3df95ce",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_player4",
    "eventList": [
        {
            "id": "85c8a005-b977-4106-88c2-ecb723fc6af8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "cb217794-cf2d-48a5-990a-1e99d3df95ce"
        },
        {
            "id": "085e44c7-bdb5-4ee9-992f-c3c60c38cd64",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "cb217794-cf2d-48a5-990a-1e99d3df95ce"
        },
        {
            "id": "e6cb8ecf-9016-476e-8192-06d64f80a9a2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "cb217794-cf2d-48a5-990a-1e99d3df95ce"
        },
        {
            "id": "f5b4ac31-b158-4828-bfa8-20b2d1c3f8a6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "3e307da7-d4fb-40db-b458-219626252223",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "cb217794-cf2d-48a5-990a-1e99d3df95ce"
        },
        {
            "id": "dd639c8f-e902-45cf-9b5b-ece30e31956d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "21b2cc7c-f48e-4996-a126-6189dfda6f1c",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "cb217794-cf2d-48a5-990a-1e99d3df95ce"
        },
        {
            "id": "8f02a9c8-4e43-4f55-89de-a4ed5d3e5ba7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "5a268622-91aa-4d6b-bb07-67d14305ab7c",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "cb217794-cf2d-48a5-990a-1e99d3df95ce"
        },
        {
            "id": "379ccd58-0a13-42a3-9d8d-4928eb8ecb8d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "cb217794-cf2d-48a5-990a-1e99d3df95ce"
        },
        {
            "id": "d824e0e4-5e11-4fa2-a151-7a0e88357ad8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "cb217794-cf2d-48a5-990a-1e99d3df95ce"
        },
        {
            "id": "d5b14b6b-e76d-4fdf-870a-83dfa9a0b7bd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "cb217794-cf2d-48a5-990a-1e99d3df95ce"
        },
        {
            "id": "c2e88806-d398-4742-9462-fe4e49199427",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "8bca24dd-3c7d-4088-b1ad-33def982ac0e",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "cb217794-cf2d-48a5-990a-1e99d3df95ce"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "96a390b5-369b-448b-bae4-1bc42e324ca3",
    "visible": true
}