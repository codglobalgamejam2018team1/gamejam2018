if not o_player.invincible_
{
	o_player.invincible_ = true;
	audio_play_sound(a_wallHitBuzz, 10, false);
	o_player.player_health_ -= 1;
	alarm[0] = global.one_second_;
}