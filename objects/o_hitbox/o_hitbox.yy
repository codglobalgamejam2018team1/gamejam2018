{
    "id": "f582135b-7b37-4746-bddc-67eb275e58ec",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_hitbox",
    "eventList": [
        {
            "id": "f47a49a2-bbb9-483b-b6d4-1e9c3ef921ad",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f582135b-7b37-4746-bddc-67eb275e58ec"
        },
        {
            "id": "89dd063a-a320-4304-9319-b3d0711ba171",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "f582135b-7b37-4746-bddc-67eb275e58ec"
        },
        {
            "id": "f29f6d10-ea05-4088-bd13-0efb1ee4ffe0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "21b2cc7c-f48e-4996-a126-6189dfda6f1c",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "f582135b-7b37-4746-bddc-67eb275e58ec"
        },
        {
            "id": "679152f8-ccd8-428c-bdd4-04cf7d35233c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "f582135b-7b37-4746-bddc-67eb275e58ec"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "6dac4494-3893-4f93-8074-ff2d20a296f1",
    "visible": false
}