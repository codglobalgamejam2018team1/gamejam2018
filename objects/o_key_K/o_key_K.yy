{
    "id": "c4327de6-d80e-4ec5-b52e-ba7a70f75f53",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_key_K",
    "eventList": [
        {
            "id": "c05da738-e7b5-4048-80ba-2de7f09bf40e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c4327de6-d80e-4ec5-b52e-ba7a70f75f53"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "e243d37d-0be7-4001-94ca-095d8c144212",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "799c4968-ed5b-466e-b82f-7d4b9631b4d1",
    "visible": true
}