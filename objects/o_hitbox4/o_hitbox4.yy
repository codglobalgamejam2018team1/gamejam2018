{
    "id": "21126d7b-5ea0-4aff-8f17-8e9ea14dc74d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_hitbox4",
    "eventList": [
        {
            "id": "3f1011b8-ccbc-4898-8a02-ffce445ccc4c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "21126d7b-5ea0-4aff-8f17-8e9ea14dc74d"
        },
        {
            "id": "233b2106-153e-430b-9708-142accd2c961",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "21126d7b-5ea0-4aff-8f17-8e9ea14dc74d"
        },
        {
            "id": "914cc483-2c56-4374-b75a-848727bab6f2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "21b2cc7c-f48e-4996-a126-6189dfda6f1c",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "21126d7b-5ea0-4aff-8f17-8e9ea14dc74d"
        },
        {
            "id": "13358cdd-8e26-4d92-b5b4-2c3015bcaead",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "21126d7b-5ea0-4aff-8f17-8e9ea14dc74d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "6dac4494-3893-4f93-8074-ff2d20a296f1",
    "visible": false
}