{
    "id": "724dce0d-9f99-44f5-92e6-940b148f71da",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_player",
    "eventList": [
        {
            "id": "49ad91f4-a12c-46ad-9088-ce304c00770c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "724dce0d-9f99-44f5-92e6-940b148f71da"
        },
        {
            "id": "a63a1a98-c43d-4ff5-953d-c54c5101043b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "724dce0d-9f99-44f5-92e6-940b148f71da"
        },
        {
            "id": "36ccf028-f3a2-440f-b65f-7468a96baef9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "724dce0d-9f99-44f5-92e6-940b148f71da"
        },
        {
            "id": "5b8ac9df-4ac8-46ac-80ce-cec37798977f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "3e307da7-d4fb-40db-b458-219626252223",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "724dce0d-9f99-44f5-92e6-940b148f71da"
        },
        {
            "id": "31150594-77a7-4115-a381-33f11c352f71",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "21b2cc7c-f48e-4996-a126-6189dfda6f1c",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "724dce0d-9f99-44f5-92e6-940b148f71da"
        },
        {
            "id": "0b40eb77-1c3d-4433-858e-44ecbaf5e225",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "5a268622-91aa-4d6b-bb07-67d14305ab7c",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "724dce0d-9f99-44f5-92e6-940b148f71da"
        },
        {
            "id": "007d9c44-1b8b-4810-9df8-1cb9fef88b61",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "724dce0d-9f99-44f5-92e6-940b148f71da"
        },
        {
            "id": "73e14841-e123-4563-a725-ddf63c14f54f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "724dce0d-9f99-44f5-92e6-940b148f71da"
        },
        {
            "id": "f494336b-1d9a-49a2-b326-6a070c36f02a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "724dce0d-9f99-44f5-92e6-940b148f71da"
        },
        {
            "id": "63a6f850-072c-4f01-a0b2-79d84c014efe",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "8bca24dd-3c7d-4088-b1ad-33def982ac0e",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "724dce0d-9f99-44f5-92e6-940b148f71da"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "96a390b5-369b-448b-bae4-1bc42e324ca3",
    "visible": true
}