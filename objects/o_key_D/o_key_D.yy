{
    "id": "56596455-a73a-4bf8-b547-ab3ba29a5a6b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_key_D",
    "eventList": [
        {
            "id": "c64c2702-5ff0-4331-92c1-7add4ce9c51c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "56596455-a73a-4bf8-b547-ab3ba29a5a6b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "e243d37d-0be7-4001-94ca-095d8c144212",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "799c4968-ed5b-466e-b82f-7d4b9631b4d1",
    "visible": true
}