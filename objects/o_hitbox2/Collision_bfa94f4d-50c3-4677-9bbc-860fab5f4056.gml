if not o_player2.invincible_
{
	o_player2.invincible_ = true;
	audio_play_sound(a_wallHitBuzz, 10, false);
	o_player2.player_health_ -= 1;
	alarm[0] = global.one_second_;
}