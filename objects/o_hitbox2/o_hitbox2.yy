{
    "id": "0c469ed6-f513-440e-b214-8333b0280d79",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_hitbox2",
    "eventList": [
        {
            "id": "e5ab46bd-b1cf-4f37-b03e-e7234668716a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0c469ed6-f513-440e-b214-8333b0280d79"
        },
        {
            "id": "39401798-b27a-4a8c-aff4-6914fab3a67a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "0c469ed6-f513-440e-b214-8333b0280d79"
        },
        {
            "id": "bfa94f4d-50c3-4677-9bbc-860fab5f4056",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "21b2cc7c-f48e-4996-a126-6189dfda6f1c",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "0c469ed6-f513-440e-b214-8333b0280d79"
        },
        {
            "id": "58426716-f495-44ba-ac17-5362f36f2990",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "0c469ed6-f513-440e-b214-8333b0280d79"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "6dac4494-3893-4f93-8074-ff2d20a296f1",
    "visible": false
}