{
    "id": "7bb1221a-0628-4663-b30b-617519b9691e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_key_L",
    "eventList": [
        {
            "id": "9975507e-2c95-45ee-aab3-330b5aa1ccb2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7bb1221a-0628-4663-b30b-617519b9691e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "e243d37d-0be7-4001-94ca-095d8c144212",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "799c4968-ed5b-466e-b82f-7d4b9631b4d1",
    "visible": true
}