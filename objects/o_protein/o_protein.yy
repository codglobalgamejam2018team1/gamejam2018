{
    "id": "5a268622-91aa-4d6b-bb07-67d14305ab7c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_protein",
    "eventList": [
        {
            "id": "30de8234-7b7b-4350-8899-4857e5c90fb2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5a268622-91aa-4d6b-bb07-67d14305ab7c"
        },
        {
            "id": "a2829009-f5e5-4a11-8563-9ea04bda95ff",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "5a268622-91aa-4d6b-bb07-67d14305ab7c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "30ca82d9-65ba-4f98-ae3a-7187764f5634",
    "visible": true
}