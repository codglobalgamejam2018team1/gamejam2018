{
    "id": "21b2cc7c-f48e-4996-a126-6189dfda6f1c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_tissue_wall",
    "eventList": [
        {
            "id": "b930f91a-5a05-4ac7-b0ef-f45d9b282d7a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "21b2cc7c-f48e-4996-a126-6189dfda6f1c"
        },
        {
            "id": "7ff45ce8-3700-4801-9d44-7e50f9e5628a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "21b2cc7c-f48e-4996-a126-6189dfda6f1c"
        },
        {
            "id": "d30a6673-99d5-4154-a52f-01cc7e6abc3e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "21b2cc7c-f48e-4996-a126-6189dfda6f1c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": true,
    "spriteId": "b070ce65-5f53-49bd-b305-28c233f9335c",
    "visible": true
}