/// @description Change to 2 cells
audio_play_sound(a_divideSplooch, 10, false);
player_protein_ = 0;
state_ = 2;
player_can_divide_ = false;
image_xscale = 4;
image_yscale = 4;
o_hitbox2.image_xscale = 4;
o_hitbox2.image_yscale = 4;
image_index = 3;
audio_play_sound(a_2cellBeatOverlay, 5, true);