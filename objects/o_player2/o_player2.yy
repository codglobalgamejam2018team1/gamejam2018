{
    "id": "e15a9dfd-46d6-45c7-b6c2-f000f439b691",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_player2",
    "eventList": [
        {
            "id": "67c6aacf-5622-4a83-9594-cdcf2429f9b5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e15a9dfd-46d6-45c7-b6c2-f000f439b691"
        },
        {
            "id": "8376667d-27a1-480b-b130-4c926ecc3b8f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e15a9dfd-46d6-45c7-b6c2-f000f439b691"
        },
        {
            "id": "ebb34837-f1c4-462d-917e-3d725495c5dd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "e15a9dfd-46d6-45c7-b6c2-f000f439b691"
        },
        {
            "id": "d732ceba-d087-4d67-8d3e-86b9cdcaf5fe",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "3e307da7-d4fb-40db-b458-219626252223",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "e15a9dfd-46d6-45c7-b6c2-f000f439b691"
        },
        {
            "id": "379445be-9d34-497b-bad5-3781c69703d3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "21b2cc7c-f48e-4996-a126-6189dfda6f1c",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "e15a9dfd-46d6-45c7-b6c2-f000f439b691"
        },
        {
            "id": "66a16403-d647-466a-9682-1803da72d2ac",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "5a268622-91aa-4d6b-bb07-67d14305ab7c",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "e15a9dfd-46d6-45c7-b6c2-f000f439b691"
        },
        {
            "id": "57d05d4d-5526-4e95-ae68-b3dd1728336a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "e15a9dfd-46d6-45c7-b6c2-f000f439b691"
        },
        {
            "id": "8fc107ab-abc8-40c3-bd98-ceff3b825236",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "e15a9dfd-46d6-45c7-b6c2-f000f439b691"
        },
        {
            "id": "6bc1789c-6b6a-4b36-a3cd-89b2bae1a9f1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "e15a9dfd-46d6-45c7-b6c2-f000f439b691"
        },
        {
            "id": "e9375802-7ca2-4239-8201-918050f33982",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "8bca24dd-3c7d-4088-b1ad-33def982ac0e",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "e15a9dfd-46d6-45c7-b6c2-f000f439b691"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "96a390b5-369b-448b-bae4-1bc42e324ca3",
    "visible": true
}