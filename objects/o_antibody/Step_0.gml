// move vertically down
move_contact_solid(270, 12);

// move horizontally in direction set on creation, until hit wall, then reverse direction

if room_get_name(room) = "r_world"
{
	if(horizdir == 1)
	{
		move_contact_solid(right_dir_, zigzag_speed_);
		if (x > 545)
		{
			horizdir = 0
		}
	}

	if(horizdir == 0)
	{
		move_contact_solid(left_dir_, zigzag_speed_);
		if (x < 350)
		{
			horizdir = 1
		}
	}
}

if (room_get_name(room) == "r_world2") or (room_get_name(room) == "r_world3")
{
	if(horizdir == 1)
	{
		move_contact_solid(right_dir_, zigzag_speed_);
		if ((x > 290) and (x < 605)) or (x > 735)
		{
			horizdir = 0
		}
	}


	if(horizdir == 0)
	{
		move_contact_solid(left_dir_, zigzag_speed_);
		if (x < 155) or ((x < 605) and (x > 290))
		{
			horizdir = 1
		}
	}
}