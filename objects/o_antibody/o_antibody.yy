{
    "id": "8bca24dd-3c7d-4088-b1ad-33def982ac0e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_antibody",
    "eventList": [
        {
            "id": "2b21241b-4359-4a1e-b862-e8480e4cb203",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "8bca24dd-3c7d-4088-b1ad-33def982ac0e"
        },
        {
            "id": "f84a656c-72d5-46b7-ab1e-d34f2ba74cc7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "8bca24dd-3c7d-4088-b1ad-33def982ac0e"
        },
        {
            "id": "a98e63d6-af42-48f4-924d-6c1b53ccc92c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8bca24dd-3c7d-4088-b1ad-33def982ac0e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "5e6b0251-ff74-4306-902a-fb16fb9a7358",
    "visible": true
}