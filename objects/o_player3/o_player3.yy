{
    "id": "ea753058-22ed-4f44-b2c7-3afad5c162e7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_player3",
    "eventList": [
        {
            "id": "f6f4496b-2a81-4cd9-bcd0-ad9065eddd13",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ea753058-22ed-4f44-b2c7-3afad5c162e7"
        },
        {
            "id": "9abee8f9-0571-4b62-9d8e-3e7e70eac72e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "ea753058-22ed-4f44-b2c7-3afad5c162e7"
        },
        {
            "id": "d03186ec-0ab6-4117-8fbd-11116e943386",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "ea753058-22ed-4f44-b2c7-3afad5c162e7"
        },
        {
            "id": "0a7d5e47-ecd7-47bd-9480-c068daab585f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "3e307da7-d4fb-40db-b458-219626252223",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "ea753058-22ed-4f44-b2c7-3afad5c162e7"
        },
        {
            "id": "70e85e41-b0a9-49b1-9fbb-0ca710c5ee87",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "21b2cc7c-f48e-4996-a126-6189dfda6f1c",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "ea753058-22ed-4f44-b2c7-3afad5c162e7"
        },
        {
            "id": "3c6883b9-6aae-4289-b64e-9f776f225c24",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "5a268622-91aa-4d6b-bb07-67d14305ab7c",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "ea753058-22ed-4f44-b2c7-3afad5c162e7"
        },
        {
            "id": "267a5326-e326-42f0-8a86-0ef0ad315fe9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "ea753058-22ed-4f44-b2c7-3afad5c162e7"
        },
        {
            "id": "30cf9c4c-ee4b-4ae9-9e6a-13c7038a0baf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "ea753058-22ed-4f44-b2c7-3afad5c162e7"
        },
        {
            "id": "fedd5fa1-709a-4c04-8c39-58a8166f259a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "ea753058-22ed-4f44-b2c7-3afad5c162e7"
        },
        {
            "id": "8627a4ae-f90f-4499-a01e-f11dc53cfb48",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "8bca24dd-3c7d-4088-b1ad-33def982ac0e",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "ea753058-22ed-4f44-b2c7-3afad5c162e7"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "96a390b5-369b-448b-bae4-1bc42e324ca3",
    "visible": true
}