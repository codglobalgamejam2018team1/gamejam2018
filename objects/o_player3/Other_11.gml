/// @description Change to 4 cells
audio_play_sound(a_divideSplooch, 10, false);
player_protein_ = 0;
state_ = 4;
player_can_divide_ = false;
image_xscale = 6;
image_yscale = 6;
o_hitbox3.image_xscale = 6;
o_hitbox3.image_yscale = 6;
image_index = 6;
audio_play_sound(a_4cellBeatOverlay, 5, true);

alarm[1] = global.one_second_;