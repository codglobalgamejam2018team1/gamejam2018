{
    "id": "e243d37d-0be7-4001-94ca-095d8c144212",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_key_base",
    "eventList": [
        {
            "id": "645578f3-32d8-424c-822c-89835c2ec47f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e243d37d-0be7-4001-94ca-095d8c144212"
        },
        {
            "id": "1ad99841-489c-4c50-8b7c-73c9d3de103e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "e243d37d-0be7-4001-94ca-095d8c144212"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "799c4968-ed5b-466e-b82f-7d4b9631b4d1",
    "visible": true
}