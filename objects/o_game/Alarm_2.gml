/// @description Spawn Antibody
 
antibody_can_spawn_ = true;

if room_get_name(room) == "r_world"
{
	instance_create_layer(random_range(400, 495), 0, "Instances", o_antibody);
}

if (room_get_name(room) == "r_world2") or (room_get_name(room) == "r_world3")
{
	instance_create_layer(random_range(205, 240), 0, "Instances", o_antibody);
	instance_create_layer(random_range(655, 685), 0, "Instances", o_antibody);
}