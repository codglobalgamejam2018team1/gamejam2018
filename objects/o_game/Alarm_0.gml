/// @description Spawn Enemy

enemy_can_spawn_ = true;

if room_get_name(room) == "r_world"
{
	instance_create_layer(random_range(350, 545), 0, "Instances", o_enemy);
}

if (room_get_name(room) == "r_world2") or (room_get_name(room) == "r_world3")
{
	instance_create_layer(random_range(155, 290), 0, "Instances", o_enemy);
	instance_create_layer(random_range(605, 735), 0, "Instances", o_enemy);
}
