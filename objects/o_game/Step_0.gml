if not instance_exists(o_enemy) and enemy_can_spawn_
{
	enemy_can_spawn_ = false;
	alarm[0] = global.one_second_;
}
if not instance_exists(o_protein) and protein_can_spawn_
{
	protein_can_spawn_ = false;
	alarm[1] = global.one_second_ * 3;
}

if not game_ending_ and global.game_can_end_
{
	game_ending_ = true;
	alarm[9] = global.one_second_ * 3;
	instance_create_layer(327, 240, "Instances", o_endgame);
}

if not instance_exists(o_antibody) and antibody_can_spawn_
{
	antibody_can_spawn_ = false;
	alarm[2] = global.one_second_ * 2;
}
if global.player_dead_ and global.player2_dead_ and global.player3_dead_ and global.player4_dead_
{
	global.game_can_end_ = true;
}
if (global.player_win_level_ and global.player2_win_level_ and global.player3_win_level_ and global.player4_win_level_) and (room_get_name(room) != "r_world4")
{
	room_goto_next();
}