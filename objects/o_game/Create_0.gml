global.one_second_ = game_get_speed(gamespeed_fps) * 2;
global.dir_left_ = 180;
global.dir_right_ = 0;
global.player_max_health_ = 3;
global.player_health_ = global.player_max_health_;
global.game_can_end_ = false;
global.player_dead_ = false;
global.player2_dead_ = true;
global.player3_dead_ = true;
global.player4_dead_ = true;
global.player_win_level_ = false;
global.player2_win_level_ = true;
global.player3_win_level_ = true;
global.player4_win_level_ = true;

enemy_can_spawn_ = true;
protein_can_spawn_ = true;
game_ending_ = false;
antibody_can_spawn_ = true;

audio_play_sound(a_lvl1mainMusic, 5, true);

// draft of method not used -- for reference in case (but probably delete-able)
// initial spawning range limits
/*
global.left_limit_1_ = 350
global.right_limit_1_ = 500
*/

alarm[3] = global.one_second_ * 3;