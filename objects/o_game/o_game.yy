{
    "id": "7166eba9-2347-4325-83ae-6efdb616b309",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_game",
    "eventList": [
        {
            "id": "f237c301-09c8-4005-8312-550606274d70",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7166eba9-2347-4325-83ae-6efdb616b309"
        },
        {
            "id": "7205e725-985d-4854-adab-5a493de3cf87",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "7166eba9-2347-4325-83ae-6efdb616b309"
        },
        {
            "id": "b4aff406-c22b-495a-b58d-f6eaf6ca8b32",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "7166eba9-2347-4325-83ae-6efdb616b309"
        },
        {
            "id": "b482c2d9-3382-4d2f-bfce-28a185f05811",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 9,
            "eventtype": 2,
            "m_owner": "7166eba9-2347-4325-83ae-6efdb616b309"
        },
        {
            "id": "ccd2f34d-ed6d-479f-b18f-2c50c59ddd1f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "7166eba9-2347-4325-83ae-6efdb616b309"
        },
        {
            "id": "e17c2939-9453-46c2-addd-1dca60207870",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 27,
            "eventtype": 9,
            "m_owner": "7166eba9-2347-4325-83ae-6efdb616b309"
        },
        {
            "id": "187832f8-bc4b-436d-b3ab-4b586d69f098",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "7166eba9-2347-4325-83ae-6efdb616b309"
        },
        {
            "id": "269a0def-1441-4e54-9f81-4b6c05faa8ac",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 3,
            "eventtype": 2,
            "m_owner": "7166eba9-2347-4325-83ae-6efdb616b309"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}