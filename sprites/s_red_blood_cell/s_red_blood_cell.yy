{
    "id": "21742cfa-0b0e-4f47-b841-3917a2857efe",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_red_blood_cell",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5e609cd0-f002-4233-b5ed-02bf0eb15b98",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "21742cfa-0b0e-4f47-b841-3917a2857efe",
            "compositeImage": {
                "id": "c4c1f92e-2233-4617-b20e-ef768ace4700",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e609cd0-f002-4233-b5ed-02bf0eb15b98",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1dcd3627-b716-465a-bbe5-30e543615796",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e609cd0-f002-4233-b5ed-02bf0eb15b98",
                    "LayerId": "9bb1cbae-9adf-4343-9577-b3269a47a460"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "9bb1cbae-9adf-4343-9577-b3269a47a460",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "21742cfa-0b0e-4f47-b841-3917a2857efe",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}