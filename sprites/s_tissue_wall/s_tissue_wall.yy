{
    "id": "b070ce65-5f53-49bd-b305-28c233f9335c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_tissue_wall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "cb2a4a6f-3778-42d5-8bb3-99d3a183ae1b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b070ce65-5f53-49bd-b305-28c233f9335c",
            "compositeImage": {
                "id": "d7d6629a-06c1-4b1a-ba9f-0883fd18c5f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cb2a4a6f-3778-42d5-8bb3-99d3a183ae1b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b20fac1f-1725-4c8b-89c3-ddc170983b1d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb2a4a6f-3778-42d5-8bb3-99d3a183ae1b",
                    "LayerId": "0a29cf49-fca7-4544-82a8-dbcacfea9c34"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "0a29cf49-fca7-4544-82a8-dbcacfea9c34",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b070ce65-5f53-49bd-b305-28c233f9335c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}