{
    "id": "4f5ff0e5-43a5-4be6-bc2f-024f889accc8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_spacebar",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 10,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "33879e8f-7749-4493-8cd5-84cbabd5967a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f5ff0e5-43a5-4be6-bc2f-024f889accc8",
            "compositeImage": {
                "id": "cdcd309a-76d4-41ba-a9d8-7c5e00b66816",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "33879e8f-7749-4493-8cd5-84cbabd5967a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce085c0f-8c59-4582-b6f6-d55a52443f14",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "33879e8f-7749-4493-8cd5-84cbabd5967a",
                    "LayerId": "45d5ebe7-a364-4967-a30c-d351b3710903"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "45d5ebe7-a364-4967-a30c-d351b3710903",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4f5ff0e5-43a5-4be6-bc2f-024f889accc8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}