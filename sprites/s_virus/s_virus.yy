{
    "id": "96a390b5-369b-448b-bae4-1bc42e324ca3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_virus",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 12,
    "bbox_left": 3,
    "bbox_right": 12,
    "bbox_top": 3,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "153e3302-5bde-4701-8a4a-544abdb76b7d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96a390b5-369b-448b-bae4-1bc42e324ca3",
            "compositeImage": {
                "id": "f1857b22-ad78-4e1e-af16-92a57925b54d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "153e3302-5bde-4701-8a4a-544abdb76b7d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "907c2c00-8e21-4944-9536-127f769ec547",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "153e3302-5bde-4701-8a4a-544abdb76b7d",
                    "LayerId": "0d729ffc-3290-43af-9a4a-8c3f42cc75d3"
                },
                {
                    "id": "15199097-8504-4b90-9059-1302494f1704",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "153e3302-5bde-4701-8a4a-544abdb76b7d",
                    "LayerId": "952728c4-0866-4699-8596-10bdec39178a"
                }
            ]
        },
        {
            "id": "7feedf04-692b-4992-8455-eaef8ccd1240",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96a390b5-369b-448b-bae4-1bc42e324ca3",
            "compositeImage": {
                "id": "2e9ffea5-87c9-4d18-9953-8917442ee85b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7feedf04-692b-4992-8455-eaef8ccd1240",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e24a152-8895-4957-a15f-06cd7ca360cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7feedf04-692b-4992-8455-eaef8ccd1240",
                    "LayerId": "0d729ffc-3290-43af-9a4a-8c3f42cc75d3"
                },
                {
                    "id": "ab16ac98-3894-4300-b0a6-ab1479176bf6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7feedf04-692b-4992-8455-eaef8ccd1240",
                    "LayerId": "952728c4-0866-4699-8596-10bdec39178a"
                }
            ]
        },
        {
            "id": "3dc14a4b-4f03-4a12-9a3f-d8a24683600a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96a390b5-369b-448b-bae4-1bc42e324ca3",
            "compositeImage": {
                "id": "37dbc6bf-746b-4dad-9bd2-5740d4617f00",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3dc14a4b-4f03-4a12-9a3f-d8a24683600a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c56da3b2-b413-41af-87bd-76a7e9ce3c05",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3dc14a4b-4f03-4a12-9a3f-d8a24683600a",
                    "LayerId": "0d729ffc-3290-43af-9a4a-8c3f42cc75d3"
                },
                {
                    "id": "1365a534-8c45-4d4a-9c36-e5fb5697512c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3dc14a4b-4f03-4a12-9a3f-d8a24683600a",
                    "LayerId": "952728c4-0866-4699-8596-10bdec39178a"
                }
            ]
        },
        {
            "id": "985c76f8-897c-4166-9b8c-3511fc047546",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96a390b5-369b-448b-bae4-1bc42e324ca3",
            "compositeImage": {
                "id": "d195e438-b019-4c46-b415-92a7156cdafd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "985c76f8-897c-4166-9b8c-3511fc047546",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "030a09fe-8f1c-4b67-8186-56b7f444c218",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "985c76f8-897c-4166-9b8c-3511fc047546",
                    "LayerId": "0d729ffc-3290-43af-9a4a-8c3f42cc75d3"
                },
                {
                    "id": "86e16030-d1fd-4a4f-99ec-14d1fc207b4c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "985c76f8-897c-4166-9b8c-3511fc047546",
                    "LayerId": "952728c4-0866-4699-8596-10bdec39178a"
                }
            ]
        },
        {
            "id": "3108eced-7652-48d3-993a-a46861415f4d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96a390b5-369b-448b-bae4-1bc42e324ca3",
            "compositeImage": {
                "id": "7f5e6d79-6f61-4bb1-b04f-6816c5fe4694",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3108eced-7652-48d3-993a-a46861415f4d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "63e0a475-616f-4680-9fee-aaf870a0ffa3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3108eced-7652-48d3-993a-a46861415f4d",
                    "LayerId": "0d729ffc-3290-43af-9a4a-8c3f42cc75d3"
                },
                {
                    "id": "936e606e-c454-4c0b-b7b8-0f3afb644871",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3108eced-7652-48d3-993a-a46861415f4d",
                    "LayerId": "952728c4-0866-4699-8596-10bdec39178a"
                }
            ]
        },
        {
            "id": "72d963cb-a55a-4e81-a012-72150c110e10",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96a390b5-369b-448b-bae4-1bc42e324ca3",
            "compositeImage": {
                "id": "6c89d182-a846-4e11-9552-6f1a9c8e11e9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "72d963cb-a55a-4e81-a012-72150c110e10",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "342a100c-b8ac-4163-bc41-6f9ea6e64a6f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "72d963cb-a55a-4e81-a012-72150c110e10",
                    "LayerId": "0d729ffc-3290-43af-9a4a-8c3f42cc75d3"
                },
                {
                    "id": "d3093c08-6cfd-43ce-a948-2900f36fbbea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "72d963cb-a55a-4e81-a012-72150c110e10",
                    "LayerId": "952728c4-0866-4699-8596-10bdec39178a"
                }
            ]
        },
        {
            "id": "21b807e1-49b3-4cf6-9e55-9e1a655d67ee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96a390b5-369b-448b-bae4-1bc42e324ca3",
            "compositeImage": {
                "id": "383d9d48-fdc9-4401-b38e-1759b6a28089",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "21b807e1-49b3-4cf6-9e55-9e1a655d67ee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a561fa4d-102c-46e0-b890-ee0e37ae33b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "21b807e1-49b3-4cf6-9e55-9e1a655d67ee",
                    "LayerId": "0d729ffc-3290-43af-9a4a-8c3f42cc75d3"
                },
                {
                    "id": "f85e2375-5648-4a8c-8b19-4b3ea6675587",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "21b807e1-49b3-4cf6-9e55-9e1a655d67ee",
                    "LayerId": "952728c4-0866-4699-8596-10bdec39178a"
                }
            ]
        },
        {
            "id": "75f61a82-14ae-4a52-ac48-fea361a57606",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96a390b5-369b-448b-bae4-1bc42e324ca3",
            "compositeImage": {
                "id": "fe96777f-7c23-46d3-ab87-76efde5aba4e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "75f61a82-14ae-4a52-ac48-fea361a57606",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc45f584-216f-4022-b388-9d7bd05ffff7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "75f61a82-14ae-4a52-ac48-fea361a57606",
                    "LayerId": "0d729ffc-3290-43af-9a4a-8c3f42cc75d3"
                },
                {
                    "id": "f27317cf-ec47-4a9a-a616-929863e25459",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "75f61a82-14ae-4a52-ac48-fea361a57606",
                    "LayerId": "952728c4-0866-4699-8596-10bdec39178a"
                }
            ]
        },
        {
            "id": "9ca9a917-9cc1-4fa7-84b6-835a6e5ed871",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96a390b5-369b-448b-bae4-1bc42e324ca3",
            "compositeImage": {
                "id": "d034a38a-c08a-44d7-b1d5-13bc9093a474",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ca9a917-9cc1-4fa7-84b6-835a6e5ed871",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d0aa280c-a0e7-42d6-8fc1-9b5037a7a357",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ca9a917-9cc1-4fa7-84b6-835a6e5ed871",
                    "LayerId": "0d729ffc-3290-43af-9a4a-8c3f42cc75d3"
                },
                {
                    "id": "7579209b-cc7f-4ba7-8938-8b423a825c37",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ca9a917-9cc1-4fa7-84b6-835a6e5ed871",
                    "LayerId": "952728c4-0866-4699-8596-10bdec39178a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "952728c4-0866-4699-8596-10bdec39178a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "96a390b5-369b-448b-bae4-1bc42e324ca3",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "0d729ffc-3290-43af-9a4a-8c3f42cc75d3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "96a390b5-369b-448b-bae4-1bc42e324ca3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 1.5,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}