{
    "id": "8ad52778-2a70-47dc-80cf-fb2a2aa123c0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_white_cell",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 2,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a122fda7-7d0b-4fb0-9537-5d094e00952f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8ad52778-2a70-47dc-80cf-fb2a2aa123c0",
            "compositeImage": {
                "id": "75425796-23cb-48bc-94e8-69af3710b69d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a122fda7-7d0b-4fb0-9537-5d094e00952f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "00c0cb22-1df4-4b66-a351-03777d50b6df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a122fda7-7d0b-4fb0-9537-5d094e00952f",
                    "LayerId": "ede242c3-3a4f-4e4e-9622-3d936b441fd0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "ede242c3-3a4f-4e4e-9622-3d936b441fd0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8ad52778-2a70-47dc-80cf-fb2a2aa123c0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}