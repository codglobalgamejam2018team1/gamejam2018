{
    "id": "6dac4494-3893-4f93-8074-ff2d20a296f1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_hitbox",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "9330eb0c-3bd8-4a57-8d6d-754555d7b003",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6dac4494-3893-4f93-8074-ff2d20a296f1",
            "compositeImage": {
                "id": "92bb4874-c166-46ac-b394-de7af442b513",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9330eb0c-3bd8-4a57-8d6d-754555d7b003",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd26ebd7-7f2c-490a-99e5-8130fb7ccecb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9330eb0c-3bd8-4a57-8d6d-754555d7b003",
                    "LayerId": "fcad84b5-d7de-49b9-b761-c2288e91d2d9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "fcad84b5-d7de-49b9-b761-c2288e91d2d9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6dac4494-3893-4f93-8074-ff2d20a296f1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}