{
    "id": "799c4968-ed5b-466e-b82f-7d4b9631b4d1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_keyboard_keys",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "b006593b-93db-446e-aa1f-8e171251d22f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "799c4968-ed5b-466e-b82f-7d4b9631b4d1",
            "compositeImage": {
                "id": "1cb579b9-c538-4138-be5a-bdcf1c35255c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b006593b-93db-446e-aa1f-8e171251d22f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "23fa941b-bbbd-4c38-8810-01dcde1130f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b006593b-93db-446e-aa1f-8e171251d22f",
                    "LayerId": "7ff19c9e-98e9-4399-9fbd-85c26cbabd17"
                }
            ]
        },
        {
            "id": "ef2fb1e8-94eb-4528-b901-f5065cfb4fd5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "799c4968-ed5b-466e-b82f-7d4b9631b4d1",
            "compositeImage": {
                "id": "ef3b30a3-077d-4c53-9212-1ab100c49a0c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef2fb1e8-94eb-4528-b901-f5065cfb4fd5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "815f019e-2d34-4c75-8301-96766b52b7ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef2fb1e8-94eb-4528-b901-f5065cfb4fd5",
                    "LayerId": "7ff19c9e-98e9-4399-9fbd-85c26cbabd17"
                }
            ]
        },
        {
            "id": "f2aa8546-4da8-476b-85c8-c976c7c7a434",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "799c4968-ed5b-466e-b82f-7d4b9631b4d1",
            "compositeImage": {
                "id": "44ac82bd-8639-4122-9e76-da6374cf3089",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f2aa8546-4da8-476b-85c8-c976c7c7a434",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c9d4bae7-46fb-4003-9f8c-e7b09feb7f61",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f2aa8546-4da8-476b-85c8-c976c7c7a434",
                    "LayerId": "7ff19c9e-98e9-4399-9fbd-85c26cbabd17"
                }
            ]
        },
        {
            "id": "4173892e-c6b9-4619-b877-0b093668e320",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "799c4968-ed5b-466e-b82f-7d4b9631b4d1",
            "compositeImage": {
                "id": "36bf3263-1697-4bc9-8493-552fb4937209",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4173892e-c6b9-4619-b877-0b093668e320",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "53fca451-d7c7-4b14-8c5e-a94b7d9db7b8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4173892e-c6b9-4619-b877-0b093668e320",
                    "LayerId": "7ff19c9e-98e9-4399-9fbd-85c26cbabd17"
                }
            ]
        },
        {
            "id": "67c4b7b0-8c57-4148-bb1b-e779346b7e49",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "799c4968-ed5b-466e-b82f-7d4b9631b4d1",
            "compositeImage": {
                "id": "1d9c194e-124f-4a0c-8524-9b6a578c6a6e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "67c4b7b0-8c57-4148-bb1b-e779346b7e49",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d9e942f3-88a0-41fa-8769-3269714045cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "67c4b7b0-8c57-4148-bb1b-e779346b7e49",
                    "LayerId": "7ff19c9e-98e9-4399-9fbd-85c26cbabd17"
                }
            ]
        },
        {
            "id": "28eef742-5a31-4bed-8565-04b16dec32f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "799c4968-ed5b-466e-b82f-7d4b9631b4d1",
            "compositeImage": {
                "id": "16839acb-fe37-47e3-bc00-adf5dae6eddb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "28eef742-5a31-4bed-8565-04b16dec32f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b1e83aa-52a0-4a40-b84a-c2dde59c64ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "28eef742-5a31-4bed-8565-04b16dec32f7",
                    "LayerId": "7ff19c9e-98e9-4399-9fbd-85c26cbabd17"
                }
            ]
        },
        {
            "id": "0db94839-5c1a-4109-8fff-f0943fa602f6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "799c4968-ed5b-466e-b82f-7d4b9631b4d1",
            "compositeImage": {
                "id": "05b253d1-9978-4f59-a2ef-dceec2bcf5c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0db94839-5c1a-4109-8fff-f0943fa602f6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f7ce18a8-6701-442a-8867-ac80c821fe0e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0db94839-5c1a-4109-8fff-f0943fa602f6",
                    "LayerId": "7ff19c9e-98e9-4399-9fbd-85c26cbabd17"
                }
            ]
        },
        {
            "id": "1ac38195-b28d-4ece-b02c-21d274606392",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "799c4968-ed5b-466e-b82f-7d4b9631b4d1",
            "compositeImage": {
                "id": "f814e2d7-f5fa-4fb8-854b-511261727762",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ac38195-b28d-4ece-b02c-21d274606392",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "82863710-a899-4bf8-8105-021399ea3191",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ac38195-b28d-4ece-b02c-21d274606392",
                    "LayerId": "7ff19c9e-98e9-4399-9fbd-85c26cbabd17"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "7ff19c9e-98e9-4399-9fbd-85c26cbabd17",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "799c4968-ed5b-466e-b82f-7d4b9631b4d1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}