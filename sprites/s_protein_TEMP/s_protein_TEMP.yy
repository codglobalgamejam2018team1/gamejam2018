{
    "id": "30ca82d9-65ba-4f98-ae3a-7187764f5634",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_protein_TEMP",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 1,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "261cc338-6e4b-471f-b613-daffadf3da95",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "30ca82d9-65ba-4f98-ae3a-7187764f5634",
            "compositeImage": {
                "id": "4ff5bdbb-4ccd-43f5-91ce-e93ff3c8a59e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "261cc338-6e4b-471f-b613-daffadf3da95",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "37ba9324-9a55-49a9-9f98-f5b2a8004839",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "261cc338-6e4b-471f-b613-daffadf3da95",
                    "LayerId": "2249e9f9-2144-4a66-80ca-8d52e9ee2f2b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "2249e9f9-2144-4a66-80ca-8d52e9ee2f2b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "30ca82d9-65ba-4f98-ae3a-7187764f5634",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}