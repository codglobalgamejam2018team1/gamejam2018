{
    "id": "5e6b0251-ff74-4306-902a-fb16fb9a7358",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_antibody",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "103642f3-8003-4a3f-9aa6-e281470c3ea8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5e6b0251-ff74-4306-902a-fb16fb9a7358",
            "compositeImage": {
                "id": "be44775e-b0b7-467b-b64b-7ff39d1b1e4a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "103642f3-8003-4a3f-9aa6-e281470c3ea8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c66e5d4b-bf9e-44dc-a10e-2f1a891d83a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "103642f3-8003-4a3f-9aa6-e281470c3ea8",
                    "LayerId": "d72b1156-a41c-4cbc-8ff4-086cfd6439c4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "d72b1156-a41c-4cbc-8ff4-086cfd6439c4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5e6b0251-ff74-4306-902a-fb16fb9a7358",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}